export class OddPostsUseCase {
  static execute(posts) {
    const isOdd = (value) => value % 2 !== 0;
    const oddPosts = posts.filter((post) => isOdd(post.id));

    return oddPosts;
  }
}
