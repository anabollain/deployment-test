import { PostsRepository } from "../repositories/posts.repository";

export class UpdatePostUseCase {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    
    const updatedPost = await repository.updatePost(postModel);

    const updatedPostModel = {
      id: updatedPost.id,
      title: updatedPost.title,
      content: updatedPost.body,
    };

    const updatedPosts = posts.map((post) =>
      post.id === updatedPostModel.id
        ? (post = updatedPostModel)
        : (post = post)
    );

    return updatedPosts;
  }
}
