import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class AddPostUseCase {
  static async execute(posts, postModel) {
    const repository = new PostsRepository();

    const newPost = await repository.addNewPost(postModel);

    const newPostModel = new Post({
      id: newPost.id,
      title: newPost.title,
      content: newPost.body,
    });

    const updatedPosts = [newPostModel, ...posts];

    return updatedPosts;
  }
}
