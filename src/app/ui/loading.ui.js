import { LitElement, html } from "lit";

export class LoadingUI extends LitElement {
  getStyles() {
    return html`
      <style>
        .loading {
          /*General properties*/
          margin-top: 10rem;
          /*Display*/
          display: flex;
          justify-content: center;
        }
      </style>
    `;
  }

  render() {
    return html`
      ${this.getStyles()}
      <p class="loading">Loading...</p>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("loading-ui", LoadingUI);
