import { LitElement, html, css } from "lit";

export class ButtonUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
      buttonContent: { type: String },
    };
  }

  getStyles() {
    return html`
      <style>
        .button {
          /*General properties*/
          cursor: pointer;
          font-size: var(--main-font-size);
          width: 6rem;
          min-height: 2.5rem;
          padding: 0.2rem 0.8rem 0.2rem 0.8rem;
          border: none;
          border-radius: 1.2rem;
          color: var(--font-white-color);
          background-color: var(--main-button-color);
        }

        .button:hover {
          /*General properties*/
          background-color: var(--site-highlight-color);
        }
      </style>
    `;
  }
  render() {
    return html`
      ${this.getStyles()}
      <button class="button">${this.buttonContent}</button>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("button-ui", ButtonUI);
