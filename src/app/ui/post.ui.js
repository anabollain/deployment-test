import { LitElement, html } from "lit";

export class PostUI extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  getStyles() {
    return html`
      <style>
        .post__detail > summary {
          /*General properties*/
          cursor: pointer;
          font-size: var(--main-font-size);
          list-style: none;
          user-select: none;
          padding-left: 0.5rem;
          padding-bottom: 1rem;
          /*Position*/
          position: relative;
        }

        .post__detail > summary::before {
          /*General properties*/
          font-size: 1.5rem;
          font-weight: bold;
          content: "+";
          color: var(--font-primary-color);
          /*Position*/
          position: absolute;
          left: -1.5rem;
          top: -0.125rem;
        }

        .post__detail[open] > summary::before {
          /*General properties*/
          font-size: 2rem;
          font-weight: bold;
          content: "-";
          color: var(--font-primary-color);
          /*Position*/
          position: absolute;
          left: -1.5rem;
          top: -0.75rem;
        }

        .post__detail > summary:hover {
          /*General properties*/
          color: var(--site-highlight-color);
        }

        .post__detail > p {
          /*General properties*/
          font-size: var(--detail-font-size);
          padding-left: 1rem;
        }
      </style>
    `;
  }

  render() {
    return html`
    ${this.getStyles()}
      <details class="post__detail">
        <summary>${this.post?.title}</summary>
        <p>${this.post?.content}</p>
      </detai>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("post-ui", PostUI);
