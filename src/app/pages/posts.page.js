import "../components/post-list.component";
import "../components/post-detail.component";

export class PostsPage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return /*html*/ `
    <style>
      posts-page {
        /*General properties*/
        width: 100%;
        height: 100%;
        /*Display*/
        display: flex;
        align-items: center;
        gap: 5rem;
      }

      #postList {
        /*General properties*/
        width: 20rem;
        height: 100%;
        border-radius: 1.2rem 1.2rem 0.5rem 1.2rem;
        background-color: var(--site-primary-color);
      }

      #postDetail {
        /*General properties*/
        width: 65rem;
        min-height: 80%;
        padding: 2rem;
        border-radius: 1.2rem;
        background-color: var(--site-primary-color);
      }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = /*html*/ `
    ${this.getStyles()}
        <post-list id="postList"></post-list>
        <post-detail id="postDetail"></post-detail>
    `;

    // Post list internal events
    const postDetailComponent = this.querySelector("#postDetail");

    this.addEventListener("select:post", (e) => {
      postDetailComponent.setPostDetailMode(e.detail.isAddMode);
      postDetailComponent.setInputValues(e.detail.clickedPost);
    });

    this.addEventListener("transform:postDetailMode", (e) => {
      postDetailComponent.setPostDetailMode(e.detail.isAddMode);
    });

    // Post detail internal events
    const postListComponent = this.querySelector("#postList");

    this.addEventListener("new:post", (e) => {
      postListComponent.addNewPost(e.detail.newPost);
    });

    this.addEventListener("update:post", (e) => {
      postListComponent.updatePost(e.detail.updatedPost);
    });

    this.addEventListener("delete:post", (e) => {
      postListComponent.deletePost(e.detail.postId);
    });
  }
}

customElements.define("posts-page", PostsPage);
