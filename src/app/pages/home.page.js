export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return /*html*/ `
    <style>
      home-page {
        /*General properties*/
        width: 100%;
        height: 100%;
        /*Display*/
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 4rem;
      }

      .home__title {
        font-size: 2rem;
      }

      .home__text {
        font-size: var(--main-font-size);
      }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = /*html*/ `
    ${this.getStyles()}
        <h2 class="home__title">Welcome to the GenK posting site</h2>
        <p class="home__text">Check the posts section to discover more...</p>
    `;
  }
}

customElements.define("home-page", HomePage);
