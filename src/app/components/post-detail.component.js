import { LitElement, html } from "lit";
import { Post } from "../../model/post";
import "../ui/button.ui";

export class PostDetail extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      isAddMode: { type: Boolean },
      postId: { type: Number },
      titleValue: { type: String },
      contentValue: { type: String },
      postModel: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.isAddMode = true;
    this.postId = null;
    this.titleValue = "";
    this.contentValue = "";
    this.postModel = new Post({
      title: "",
      content: "",
    });
  }

  getStyles() {
    return html`
      <style>
        .main__detail {
          /*General properties*/
          height: 100%;
          width: 100%;
          /*Display*/
          display: flex;
          flex-direction: column;
          gap: 3.5rem;
        }

        .main__detail--title {
          /*General properties*/
          font-size: var(--title-font-size);
          text-transform: capitalize;
        }

        .main__detail--form {
          /*General properties*/
          margin-left: 2rem;
          margin-right: 2rem;
          min-height: 80%;
          align-self: center;
          /*Display*/
          display: flex;
          flex-direction: column;
          align-items: space-between;
          justify-content: center;
          gap: 2.5rem;
        }

        .form__section {
          /*General properties*/
          width: 100%;
          /*Display*/
          display: flex;
          justify-content: space-between;
          gap: 5rem;
        }

        .form__section label {
          color: var(--font-primary-color);
          font-size: var(--main-font-size);
          font-weight: 600;
        }

        .form__section textarea {
          /*General properties*/
          min-width: 30rem;
          font-family: "Open Sans", sans-serif;
          min-height: 2.5rem;
          height: fit-content;
          padding: 1rem;
          border: none;
          border-radius: 0.5rem;
          resize: vertical;
        }

        .form__footer {
          /*General properties*/
          width: 30rem;
          min-height: 2.5rem;
          align-self: flex-end;
          /*Display*/
          display: flex;
          flex-direction: ${this.isAddMode ? html`row-reverse` : html`row`};
          justify-content: ${this.isAddMode
            ? html`flex-start`
            : html`flex-end`};
          gap: 4rem;
        }
      </style>
    `;
  }

  render() {
    return html`
      ${this.getStyles()}
      <section class="main__detail">
        <header>
          <h2 class="main__detail--title">Post detail</h2>
        </header>
        <form class="main__detail--form" @submit="${this.submitForm}">
          <div class="form__section">
            <label for="postTitle">Title</label>
            <textarea
              id="postTitle"
              type="text"
              name="title"
              @change="${this.setTitleInput}"
              .value="${this.titleValue}"
              placeholder="Set your post's title here"
            ></textarea>
          </div>
          <div class="form__section">
            <label for="postContent">Content</label>
            <textarea
              id="postContent"
              type="text"
              name="content"
              @change="${this.setContentInput}"
              .value="${this.contentValue}"
              placeholder="You can add your content here"
            ></textarea>
          </div>
          <footer class="form__footer">
            ${this.isAddMode
              ? html`
                  <button-ui
                    @click="${this.clickAddPost}"
                    buttonContent="Add"
                  ></button-ui>
                  <button-ui
                    @click="${this.clickCancelPost}"
                    buttonContent="Cancel"
                  ></button-ui>
                `
              : html`
                  <button-ui
                    @click="${this.clickCancelPost}"
                    buttonContent="Cancel"
                  ></button-ui>
                  <button-ui
                    @click="${this.clickUpdatePost}"
                    buttonContent="Update"
                  ></button-ui>
                  <button-ui
                    @click="${this.clickDeletePost}"
                    buttonContent="Delete"
                  ></button-ui>
                `}
          </footer>
        </form>
      </section>
    `;
  }

  // Internal methods
  // Get input values from user
  setTitleInput(e) {
    this.titleValue = e.target.value;
  }
  setContentInput(e) {
    this.contentValue = e.target.value;
  }

  // Reset inputs to original state
  resetForm() {
    this.titleValue = "";
    this.contentValue = "";
  }

  // Prevent page refresh
  submitForm(e) {
    e.preventDefault();
  }

  // Reset form
  clickCancelPost() {
    this.resetForm();
    this.isAddMode = true;
  }

  // Methods that transfer information by events to parent component
  // Method to add a new post
  clickAddPost() {
    const postModel = new Post({
      title: this.titleValue,
      content: this.contentValue,
    });

    const addNewPost = new CustomEvent("new:post", {
      bubbles: true,
      detail: {
        newPost: postModel,
      },
    });
    this.dispatchEvent(addNewPost);

    this.resetForm();
  }

  // Method to update a post from the list
  clickUpdatePost() {
    const postModel = new Post({
      id: this.postId,
      title: this.titleValue,
      content: this.contentValue,
    });

    const updatePost = new CustomEvent("update:post", {
      bubbles: true,
      detail: {
        updatedPost: postModel,
      },
    });
    this.dispatchEvent(updatePost);

    this.resetForm();
  }

  // Method to delete a post from the list
  clickDeletePost() {
    const postId = this.postId;

    const deletePost = new CustomEvent("delete:post", {
      bubbles: true,
      detail: {
        postId: postId,
      },
    });
    this.dispatchEvent(deletePost);

    this.resetForm();
  }

  // Methods executed by parent's event listeners
  // Update footer buttons
  setPostDetailMode(value) {
    this.isAddMode = value;
    this.resetForm();
  }

  // Get input values with clicked post
  setInputValues(post) {
    this.postId = post.id;
    this.titleValue = post.title;
    this.contentValue = post.content;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("post-detail", PostDetail);
