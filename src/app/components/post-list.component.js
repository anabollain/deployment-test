import { LitElement, html } from "lit";
import { AllPostsUseCase } from "../../usecases/all-posts.usecase";
import { AddPostUseCase } from "../../usecases/add-post.usecase";
import { UpdatePostUseCase } from "../../usecases/update-post.usecase";
import { DeletePostUseCase } from "../../usecases/delete-post.usecase";
import "../ui/loading.ui";
import "../ui/post.ui";
import "../ui/button.ui";

export class PostListComponent extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
      isAddMode: { type: Boolean },
      clickedPost: { type: Object },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.posts = await AllPostsUseCase.execute();
    this.isAddMode = true;
  }

  getStyles() {
    return html`
    <style>
      .main__list{
        /*General properties*/
        height: 100%;
        padding-left: 1.5rem 0rem 1.5rem 0rem;
        /*Display*/
        display: flex;
        flex-direction: column;
        gap: 2rem;
      }

      .list__header {
        /*General properties*/
        height: 10%;
        padding: 1.5rem 0rem 1.5rem 1.5rem;
        /*Position*/
        position: relative;
      }
     
      .list__header--title{
        /*General properties*/
        text-transform: capitalize;
        font-size: var(--title-font-size);
        color: var(--font-primary-color);
      }

      .list__items {
        /*General properties*/
        list-style: none;
        height: 90%;
        overflow-y: scroll;
        padding: 0rem 2rem 2rem 3rem;
        /*Display*/
        display: flex;
        flex-direction: column;
        gap: 1rem;
      }

      /*Customized scrollbar*/
      /*Width*/
      .list__items::-webkit-scrollbar{
        width: 1rem;
      }
      /*Track*/
      .list__items::-webkit-scrollbar-track{
        box-shadow: inset 0 0 1rem white; 
        border-radius: 1rem;
      }
      /*Handle*/
      .list__items::-webkit-scrollbar-thumb{
        background: var(--site-secondary-color);
        border-radius: 1rem;
      }
      /*Handle on hover*/
      .list__items::-webkit-scrollbar-thumb:hover{
        background: var(--site-highlight-color); 
      }

      .add__btn {
        /*Position*/
        position: absolute;
        top: 0;
        right: 0;
      }
    </style>
    `;
  }

  render() {
    return html`
    ${this.getStyles()}
      <section class="main__list">
        <header class="list__header">
          <h2 class="list__header--title">Posts list</h2>
          <button-ui class="add__btn" @click="${this.setAddModal}" buttonContent="Add"></button-ui>
        </header>
        ${this.posts === undefined
          ? html` <loading-ui></loading-ui> `
          : html`
              <ul class="list__items">
                ${this.posts?.map(
                  (post) =>
                    html`<li @click="${this.selectPost}" id="${post.id}" role="listitem" class="list__item">
                      <post-ui .post="${post}"></post-ui>
                    </li>`
                )}
              </ul>
            `}
      </section>
    `;
  }

  // Methods that transfer information by events to parent component
  selectPost(e) {
    const clickedPost = this.posts?.filter(
      (post) => post.id === parseInt(e.currentTarget.id)
    )[0];

    const selectPost = new CustomEvent("select:post", {
      bubbles: true,
      detail: {
        isAddMode: false,
        clickedPost: clickedPost,
      },
    });
    this.dispatchEvent(selectPost);
  }

  setAddModal() {
    const transformPostDetailMode = new CustomEvent(
      "transform:postDetailMode",
      {
        bubbles: true,
        detail: {
          isAddMode: true,
        },
      }
    );
    this.dispatchEvent(transformPostDetailMode);
  }

  // Methods executed by parent's event listeners
  async addNewPost(newPost) {
    const updatedPosts = await AddPostUseCase.execute(this.posts, newPost);
    this.posts = updatedPosts;
  }

  async updatePost(updatedPost) {
    const updatedPosts = await UpdatePostUseCase.execute(
      this.posts,
      updatedPost
    );
    this.posts = updatedPosts;
  }

  async deletePost(postId) {
    const updatedPosts = await DeletePostUseCase.execute(this.posts, postId);
    this.posts = updatedPosts;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("post-list", PostListComponent);
