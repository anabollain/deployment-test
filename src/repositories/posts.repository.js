import axios from "axios";

export class PostsRepository {
  async getAllPosts() {
    return await (
      await axios.get("https://jsonplaceholder.typicode.com/posts")
    ).data;
  }

  async addNewPost(postModel) {
    const postDto = {
      title: postModel.title,
      body: postModel.content,
      userId: 1,
    };
    return await (
      await axios.post("https://jsonplaceholder.typicode.com/posts", postDto, {
        headers: { "Content-type": "application/json; charset=UTF-8" },
      })
    ).data;
  }

  async deletePost(postId) {
    return await axios.delete(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    );
  }

  async updatePost(postModel) {
    const postDto = {
      id: postModel.id,
      title: postModel.title,
      body: postModel.content,
      userId: 1,
    };
    return await (
      await axios.put(
        `https://jsonplaceholder.typicode.com/posts/${postDto.id}`,
        postDto,
        {
          headers: { "Content-type": "application/json; charset=UTF-8" },
        }
      )
    ).data;
  }
}
