import './styles/reset.css';
import './styles/main.css';

import { Router } from "@vaadin/router";
import "./app/pages/home.page"
import "./app/pages/posts.page";

const outlet = document.querySelector("#outlet");
const router = new Router(outlet);

router.setRoutes([
    { path: "/", component: "home-page" },
    { path: "/posts", component: "posts-page" },
    { path: "(.*)", redirect: "/" },
]);