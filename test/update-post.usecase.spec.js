import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { UpdatePostUseCase } from "../src/usecases/update-post.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Update post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts including updated one", async () => {
    const postModel = new Post({
      id: 1,
      title: "New title",
      content: "This is a new content.",
    });

    PostsRepository.mockImplementation(() => {
      return {
        updatePost: () => {
          return {
            id: postModel.id,
            title: postModel.title,
            body: postModel.content,
            userId: 1,
          };
        },
      };
    });

    const POSTS = [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        userId: 1,
        id: 2,
        title: "qui est esse",
        content:
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];

    const updatedPosts = await UpdatePostUseCase.execute(POSTS, postModel);

    expect(updatedPosts.length).toBe(POSTS.length);

    expect(updatedPosts[0].id).toBe(postModel.id);
    expect(updatedPosts[0].title).toBe(postModel.title);
    expect(updatedPosts[0].content).toBe(postModel.content);
  });
});
