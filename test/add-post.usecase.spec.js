import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { AddPostUseCase } from "../src/usecases/add-post.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Add new post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get all posts including new one", async () => {
    const POSTS = [
      {
        userId: 1,
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
    ];

    const postModel = new Post({
      title: "New post",
      content: "This is a new post.",
    });

    PostsRepository.mockImplementation(() => {
      return {
        addNewPost: () => {
          return {
            id: 101,
            title: postModel.title,
            body: postModel.content,
            userId: 1,
          };
        },
      };
    });

    const updatedPosts = await AddPostUseCase.execute(POSTS, postModel);

    expect(updatedPosts.length).toBe(POSTS.length + 1);

    expect(updatedPosts[0].title).toBe(postModel.title);
    expect(updatedPosts[0].content).toBe(postModel.content);
  });
});
